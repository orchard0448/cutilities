// --------------------------------------------------------------------------------
// CsvObj
//
// Written for interfacing with csv files.
//
// --------------------------------------------------------------------------------
//    Aidan Orchard 01/28/2018
// initial commit. includes basic read_csv and field accessors dfield() sfield()
// --------------------------------------------------------------------------------


// Standard Libraries
#include <vector>
#include <string>
#include <stdexcept>
#include <fstream>
using namespace std;

// Header File
#include "CsvObj.h"

// --------------------------------------------------------------------------------
//   ####   ####  #    #  ####  ##### #####  #    #  ####  #####  ####  #####   ####
//  #    # #    # ##   # #        #   #    # #    # #    #   #   #    # #    # #
//  #      #    # # #  #  ####    #   #    # #    # #        #   #    # #    #  ####
//  #      #    # #  # #      #   #   #####  #    # #        #   #    # #####       #
//  #    # #    # #   ## #    #   #   #   #  #    # #    #   #   #    # #   #  #    #
//   ####   ####  #    #  ####    #   #    #  ####   ####    #    ####  #    #  ####
// --------------------------------------------------------------------------------

// ================
// Constructor
// ================
CsvObj::CsvObj()
{
   nfields = 0;
   return;
}

// -----------------------------------------------------------------------------
//    ##    ####   ####  ######  ####   ####   ####  #####   ####
//   #  #  #    # #    # #      #      #      #    # #    # #
//  #    # #      #      #####   ####   ####  #    # #    #  ####
//  ###### #      #      #           #      # #    # #####       #
//  #    # #    # #    # #      #    # #    # #    # #   #  #    #
//  #    #  ####   ####  ######  ####   ####   ####  #    #  ####
// -----------------------------------------------------------------------------

// ================
// dfield
// ================
std::vector <double>& CsvObj::dfield(const std::string &fieldname)
{
   // returns referenced vector by reference
   //    or nothing if it's not there

   // look for fieldname in doubles
   for(unsigned int di = 0; di < dheaders.size() ; di++)
   {
      if(fieldname == dheaders[di])
      {
         return ddata[di];
      }
   }

   // check for wrong type
   for(unsigned int si = 0; si < sheaders.size(); si++)
   {
      if(fieldname == sheaders[si])
      {
         string error = "Error in CsvObj::dfield(): field "+fieldname+" is not a double";
         throw runtime_error(error);
      }
   }

   // else no exist
   string error = "Error in QuickStruct::dfield(): field "+fieldname+" no exist";
   throw runtime_error(error);
}

// ================
// sfield
// ================
std::vector <string>& CsvObj::sfield(const std::string &fieldname)
{
   // returns referenced vector by reference
   //    or nothing if it's not there

   // look for fieldname in strings
   for(unsigned int di = 0; di < sheaders.size() ; di++)
   {
      if(fieldname == sheaders[di])
      {
         return sdata[di];
      }
   }

   // check for wrong type
   for(unsigned int si = 0; si < dheaders.size(); si++)
   {
      if(fieldname == dheaders[si])
      {
         string error = "Error in QuickStruct::sfield(): field "+fieldname+" is not a string";
         throw runtime_error(error);
      }
   }

   // else no exist
   string error = "Error in QuickStruct::sfield(): field "+fieldname+" does no exist";
   throw runtime_error(error);
}

// -----------------------------------------------------------------------------
//  #    # #    # #####   ##   #####  ####  #####   ####
//  ##  ## #    #   #    #  #    #   #    # #    # #
//  # ## # #    #   #   #    #   #   #    # #    #  ####
//  #    # #    #   #   ######   #   #    # #####       #
//  #    # #    #   #   #    #   #   #    # #   #  #    #
//  #    #  ####    #   #    #   #    ####  #    #  ####
// -----------------------------------------------------------------------------

// ================
// clear
// ================
void CsvObj::clear()
{
   // Clear Private Variables
   nfields 	= 0;
   dheaders.clear();
   ddata.clear();
   dorder.clear();
   sheaders.clear();
   sdata.clear();
   sorder.clear();
   return;
}

// -------------------------------------------------------------
//  ###### # #      ######       #  ####
//  #      # #      #            # #    #
//  #####  # #      #####        # #    #
//  #      # #      #            # #    #
//  #      # #      #            # #    #
//  #      # ###### ######       #  ####
// -------------------------------------------------------------

// ================
// read_csv
// ================
void CsvObj::read_csv(const string &filepath)
{

   // -------------------------------------------------------------
   // Constants
   // -------------------------------------------------------------

   // Max Input Stream Size
   const int MAX_INPUT_STREAM 	= 100000;

   // -------------------------------------------------------------
   // Declare
   // -------------------------------------------------------------

   char           char_header_record[MAX_INPUT_STREAM];
   char           char_data_record[MAX_INPUT_STREAM];
   string         header_record, data_record;
   int            num_rows;

   vector <int>         inds;
   vector <double>      temp_dvec;
   vector <string>      headers, data, temp_svec;
   string::size_type    sz;

   // -------------------------------------------------------------
   // Prep Object
   // -------------------------------------------------------------

   // Clear All Data
   clear();

   // -------------------------------------------------------------
   // Open CSV
   // -------------------------------------------------------------

   // exists
   if(!csv_exists(filepath))
   {
      string error = "Error in CsvObj::read_csv(): csv does not exist: " + filepath;
      throw runtime_error(error);
   }

   // Open File for Reading
   const char * CCSV_PATH = filepath.c_str();
   ifstream fin;
   fin.open(CCSV_PATH, std::ifstream::in);

   // Check Failures
   if(fin.fail())
   {
      string error = "Error in CsvObj::read_csv(): npas fail: "+ filepath;
      throw runtime_error(error);
   }
   if(!fin.good())
   {
      string error = "Error in CsvObj::read_csv(): npas good: "+ filepath;
      throw runtime_error(error);
   }

   // -------------------------------------------------------------
   // Read Header
   // -------------------------------------------------------------

   // pull header record
   fin.getline(char_header_record,MAX_INPUT_STREAM);
   header_record 	= char_header_record;
   inds 		= comma_indices(header_record);
   headers 	= string_split(header_record,inds);

   // assume all fields strings until proven guilty
   sheaders 	= headers;
   nfields 			= headers.size();
   for(unsigned int fi = 1; fi <= nfields; fi++)
   {
      sorder.push_back(fi);
   }

   // -------------------------------------------------------------
   // Pull Data
   // -------------------------------------------------------------

   // create blank vectors
   for(unsigned int si = 0; si < sheaders.size(); si++)
   {
      sdata.push_back(vector<string>());
   }

   // add lines until there are none to add
   num_rows    = 0;
   while(fin.getline(char_data_record,MAX_INPUT_STREAM))
   {
      // Convert
      data_record 	= char_data_record;

      if(data_record.size() > 0)
      {
         num_rows ++;

         // break apart the record
         inds 	= comma_indices(data_record);
         data = string_split(data_record,inds);

         //  make sure there were enough commas
         if(data.size() != nfields)
         {
            // check for trailing comma
            if(data.size() == nfields + 1 && data[data.size()-1].length() == 0)
            {
               data.pop_back();
            }
            else
            {
               // otherwise we got an issue
               string error = "Error in CsvObj::read_csv(): we hit an uneven row";
               throw runtime_error(error);
            }
         }

         // add to string data
         for(unsigned int fi = 1; fi <= nfields; fi ++)
         {
            sdata[fi-1].push_back(data[fi-1]);
         }
      }
   } // end while

   // close file
   fin.close();

   // -------------------------------------------------------------
   // Remove Trailing Strings
   // -------------------------------------------------------------

   for(unsigned int si = 0; si < sheaders.size(); si++)
   {
      for(int sj = sdata[si].size()-1; sj>= 0; sj--)
      {
         if( sdata[si][sj] == "" )
         {
            sdata[si].erase( sdata[si].begin() + sj);
         }
         else break;
      }
   }

   // -------------------------------------------------------------
   // Convert Numbers
   // -------------------------------------------------------------
   for( int si = sheaders.size()-1; si >= 0; si--)
   {
      temp_svec.clear();
      temp_dvec.clear();
      string sfield = sheaders[si];

      // pull data from this field
      temp_svec   = sdata[si];

      // if it's empty it doesn't really matter
      if(temp_svec.size() > 0)
      {


         if(all_numbers( temp_svec))
         {
            // move header, change order
            dheaders.push_back(sfield);
            sheaders.erase(sheaders.begin() + si);
            dorder.push_back(sorder[si]);
            sorder.erase(sorder.begin() + si);

            // move data
            for(unsigned int di = 0; di < temp_svec.size(); di++)
            {
               temp_dvec.push_back(std::stod(temp_svec[di].c_str()));
            }
            ddata.push_back(temp_dvec);
            sdata.erase(sdata.begin() + si);
         }
      } // end empty if

   } // end field loop

   return;
} // end read_csv

// -------------------------------------------------------------
// #    # ###### #      #####  ###### #####   ####
// #    # #      #      #    # #      #    # #
// ###### #####  #      #    # #####  #    #  ####
// #    # #      #      #####  #      #####       #
// #    # #      #      #      #      #   #  #    #
// #    # ###### ###### #      ###### #    #  ####
// -------------------------------------------------------------

// ================
// csv_exists
// ================
bool CsvObj::csv_exists(const string &filepath)
{
   const char* path = filepath.c_str();

   ifstream ifile(path);
   return static_cast<bool>(ifile);
}

// ================
// comma_indices
// ================
vector <int> CsvObj::comma_indices(const string &str)
{
   vector <int> indices;
   for(unsigned int si = 0; si < str.length(); si++)
   {
      if(str[si] == ',')
      {
         indices.push_back(si);
      }
   }
   return indices;
}

// ================
// string_split
// ================
vector <string> CsvObj::string_split(const string &str, const vector <int> &inds)
{
   // declare vars
   int      i1, i2;
   string   temp;
   vector <string> split_strings;

   // split
   if( inds.size() > 0)
   {
      temp 	= str.substr(0,inds[0]);
      split_strings.push_back(temp);
   }
   else
   {
      split_strings.push_back(str);
      return split_strings;
   }
   if(inds.size() > 1)
   {
      for(unsigned int vi = 0; vi < inds.size() - 1; vi++)
      {
         i1 	= inds[vi];
         i2 	= inds[vi+1];
         temp 	= str.substr(i1+1,i2-i1-1);
         split_strings.push_back(temp);
      }
   }
   temp 	= str.substr(inds[inds.size() - 1] +1, str.length() - inds[inds.size()-1]);
   split_strings.push_back(temp);

   return split_strings;
}

// ================
// is_numeric
// ================
bool CsvObj::is_numeric(const string &s)
{
   // first, check for the string "nan"
   if(s == "nan")
   {
      return true;
   }

   // number check
   // not sure what's happening here the internet said to do this
   if(s.empty() || std::isspace(s[0]) || std::isalpha(s[0]))
   {
      return false ;
   }
   char * p ;
   strtod(s.c_str(), &p) ;
   return (*p == 0) ;
}

// ================
// all_numbers
// ================
bool CsvObj::all_numbers(const vector <string> &v)
{
   for(unsigned int vi = 0; vi < v.size(); vi++)
   {
      if(!is_numeric(v[vi]))
      {
         return false;
      }
   }
   return true;
}


