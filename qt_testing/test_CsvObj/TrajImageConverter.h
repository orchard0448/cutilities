// --------------------------------------------------------------------------------
// TrajImageConverter
//
// Written for converting images - appending them with a plot
//
// --------------------------------------------------------------------------------
//    Aidan Orchard 01/31/2018
// initial commit.
// --------------------------------------------------------------------------------

#ifndef TRAJIMAGECONVERTER_H
#define TRAJIMAGECONVERTER_H

// Qt libraries

#include <QMainWindow>
#include <QPainter>
#include <QPixmap>
#include <QLabel>
#include <QDockWidget>
#include <QFrame>
#include <QGridLayout>

#include <QtCharts>

  using namespace QtCharts;

// normal C++ libraries
#include <string>

// --------------------------------------------------------------
// TrajImageConverter Class Declaration
// --------------------------------------------------------------
// functions:
// ----------Constructors---------------------
// Default Constructor
// ----------Accessor Functions---------------------
// ----------Mutator Functions----------------------
// ----------File IO Functions-----------------------
// --------------------------------------------------------------

class TrajImageConverter : public QWidget
{
   Q_OBJECT

private:
   // main image
   QPixmap     *main_image;
   QLabel      *imageLabel;

   // main plot
   QChartView  *main_plot;

public:
   // Default Constructor
   explicit TrajImageConverter(QWidget *parent = nullptr);

   // load_image
   void load_image(const std::string &fileName);

   // creat plots
   void create_plots();


signals:

public slots:
};

#endif // TRAJIMAGECONVERTER_H










































