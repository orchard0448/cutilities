// --------------------------------------------------------------------------------
// CsvObj
//
// Written for interfacing with csv files.
//
// --------------------------------------------------------------------------------
//    Aidan Orchard 01/28/2018
// initial commit. includes basic read_csv and field accessors dfield() sfield()
// --------------------------------------------------------------------------------

#ifndef CSVOBJ_H
#define CSVOBJ_H

// --------------------------------------------------------------
// CsvObj Class Declaration
// --------------------------------------------------------------
// functions:
// ----------Constructors---------------------
// Default Constructor
// dfield
// sfield
// ----------Mutators----------------------
// clear
// ----------File IO Functions-----------------------
// read_csv
// --------------------------------------------------------------

class CsvObj
{
   private:

      unsigned int                              nfields;

      // String Fields
      std::vector <std::string> 						sheaders;
      std::vector <std::vector <std::string>>   sdata;
      std::vector <int> 								sorder;

      // Double Fields
      std::vector <std::string> 						dheaders;
      std::vector <std::vector <double>>        ddata;
      std::vector <int> 								dorder;


   public:

      // ----------Constructors----------------------

      // Default Constructor
      CsvObj();

      // ----------Accessors----------------------

      // dfield
      std::vector <double>& dfield(const std::string &fieldname);

      // sfield
      std::vector <string>& sfield(const std::string &fieldname);

      // ----------Mutators----------------------

      // clear
      void clear();

      // ----------File IO Functions-----------------------

      // read_csv
      void read_csv(const std::string &filepath);

      // ----------Helpers-----------------------

      // csv_exists
      bool csv_exists(const std::string &filepath);

      // comma_indices
      std::vector <int> comma_indices(const std::string &str);

      // string_split
      std::vector <string> string_split(const std::string &str, const std::vector <int> &inds);

      // is_numeric
      bool is_numeric(const std::string &s);

      // all_numbers
      bool all_numbers(const std::vector <std::string> &v);

}; // end class declaration

#endif // CSVOBJ_H
