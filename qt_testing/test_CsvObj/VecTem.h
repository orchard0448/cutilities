// --------------------------------------------------------------------------------
// VectTem.h
//
// vector template
// basic functions for any length vector
//
// --------------------------------------------------------------------------------
//    Aidan Orchard 01/28/2018
// initial commit. includes vertical display dispV
// --------------------------------------------------------------------------------

#ifndef VECTEM_H
#define VECTEM_H

#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;

namespace VecTem
{
   // --------------------------------------------------
   // #####  ######  ####  #        ##   #####  ######
   // #    # #      #    # #       #  #  #    # #
   // #    # #####  #      #      #    # #    # #####
   // #    # #      #      #      ###### #####  #
   // #    # #      #    # #      #    # #   #  #
   // #####  ######  ####  ###### #    # #    # ######
   // --------------------------------------------------

   // dispV
   template <typename T>
   void dispV(const vector <T> &v);

   // --------------------------------------------------
   // #####  ###### ###### # #    # ######
   // #    # #      #      # ##   # #
   // #    # #####  #####  # # #  # #####
   // #    # #      #      # #  # # #
   // #    # #      #      # #   ## #
   // #####  ###### #      # #    # ######
   // --------------------------------------------------

   // dispV
   template <typename T>
   void dispV(const vector <T> &v)
   {
      for(int vi = 0; vi < v.size(); vi++)
      {
         cout<<v[vi]<<endl;
      }
   }
}

#endif // VECTEM_H
