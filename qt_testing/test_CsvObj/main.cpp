// Qt Libraries
#include <QCoreApplication>
#include <QApplication>
#include <QWidget>

// normal C++ libraries
#include <iostream>
using namespace std;

// Personal Libraries
#include <VecTem.h>
#include <CsvObj.h>
#include <TrajImageConverter.h>

// MAIN
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Read csv
    CsvObj  struct_in;
    struct_in.read_csv("C:\\Users\\Aidan\\Desktop\\Book1.csv");


    // create widget
    TrajImageConverter main_window;

    // load image
    main_window.load_image("C:\\Users\\Aidan\\Desktop\\krug.jpg");

    // show window
    main_window.show();

    // save window
    //main_window.grab().save("C:\\Users\\Aidan\\Desktop\\krug2.jpg");

    // load image
    //main_window.load_image("C:\\Users\\Aidan\\Desktop\\krug2.jpg");

    // save window
    //main_window.grab().save("C:\\Users\\Aidan\\Desktop\\krug3.jpg");

    return app.exec();
}
