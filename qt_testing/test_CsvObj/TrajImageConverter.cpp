// --------------------------------------------------------------------------------
// TrajImageConverter
//
// Written for converting images - appending them with a plot
//
// --------------------------------------------------------------------------------
//    Aidan Orchard 01/28/2018
// initial commit.
// --------------------------------------------------------------------------------

// Qt libs
#include <QtCharts/QLineSeries>



// standard libraries
#include <string>
using namespace std;

// header file
#include "TrajImageConverter.h"


// --------------------------------------------------------------------------------
//   ####   ####  #    #  ####  ##### #####  #    #  ####  #####  ####  #####   ####
//  #    # #    # ##   # #        #   #    # #    # #    #   #   #    # #    # #
//  #      #    # # #  #  ####    #   #    # #    # #        #   #    # #    #  ####
//  #      #    # #  # #      #   #   #####  #    # #        #   #    # #####       #
//  #    # #    # #   ## #    #   #   #   #  #    # #    #   #   #    # #   #  #    #
//   ####   ####  #    #  ####    #   #    #  ####   ####    #    ####  #    #  ####
// --------------------------------------------------------------------------------

// Default Constructor
TrajImageConverter::TrajImageConverter(QWidget *parent) : QWidget(parent)
{
   // create layout
   QGridLayout *baseLayout = new QGridLayout();

   // create image label
   imageLabel  = new imageLabel();


   // add image to left
   baseLayout->addWidget(imageLabel, 0, 0, 2, 2);

   // add plot to layout
   baseLayout->addWidget(main_plot, 0,2,2,1);

   // set the layout
   setLayout(baseLayout);
}

// -----------------------------------------------------------------------------
//  #    # #    # #####   ##   #####  ####  #####   ####
//  ##  ## #    #   #    #  #    #   #    # #    # #
//  # ## # #    #   #   #    #   #   #    # #    #  ####
//  #    # #    #   #   ######   #   #    # #####       #
//  #    # #    #   #   #    #   #   #    # #   #  #    #
//  #    #  ####    #   #    #   #    ####  #    #  ####
// -----------------------------------------------------------------------------

void TrajImageConverter::load_image(const string &fileName)
{
    // use load image function
    main_image = new QPixmap(QString(fileName.c_str()));

    // add pixmap to label
    imageLabel.setPixmap(main_image->scaledToWidth(2000));

}

// creat plots
void TrajImageConverter::create_plots()
{


}

